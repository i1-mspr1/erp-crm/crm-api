const chai = require("chai");
const chaiHttp = require("chai-http");
chai.use(chaiHttp);

class TestResponseUtils {
    static parseJsonReponse(response) {
        return JSON.parse(response);
    }
}

module.exports = TestResponseUtils;
