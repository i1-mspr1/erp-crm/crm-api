const chai = require("mocha");
const before = chai.before;
const after = chai.after;

before(async () => {
    console.log("Launching the unit tests...")
});

after(() => {
    console.log("End of unit test");
});