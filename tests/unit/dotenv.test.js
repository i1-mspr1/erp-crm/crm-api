const chai = require("chai");
const expect = chai.expect;

const chaiHttp = require("chai-http");
chai.use(chaiHttp);

const mocha = require("mocha");
const { should } = require("chai");

const connectDb = require("../../src/db/config");

mocha.describe("Test the .env file", () => {
    mocha.it("It should return a valid .env", (done) => {
        // Given
        require("dotenv").config();
        // When
        const DOT_ENV = {
            CRM_DB_USER: process.env.CRM_DB_USER,
            CRM_DATABASE: process.env.CRM_DATABASE,
            CRM_DB_PASSWORD: process.env.CRM_DB_PASSWORD,
            CRM_DB_PORT: process.env.CRM_DB_PORT,
            CRM_DB_HOST: process.env.CRM_DB_HOST,
        };
        should(DOT_ENV);
        // Then
        expect(DOT_ENV.CRM_DB_USER).should.to.not.be.equal(undefined);
        expect(DOT_ENV.CRM_DATABASE).should.not.be.equal(undefined);
        expect(DOT_ENV.CRM_DB_PASSWORD).should.not.be.equal(undefined);
        expect(DOT_ENV.CRM_DB_PORT).should.not.be.equal(undefined);
        expect(DOT_ENV.CRM_DB_HOST).should.not.be.equal(undefined);

        done();
    });
    mocha.it("It should return an error when the .env is empty", (done) => {
        // Given
        const EMPTY_DOT_ENV = {
            CRM_DB_USER: undefined,
            CRM_DATABASE: undefined,
            CRM_DB_PASSWORD: undefined,
            CRM_DB_PORT: undefined,
            CRM_DB_HOST: undefined,
        };
        // When
        const DOT_ENV = {
            CRM_DB_USER: EMPTY_DOT_ENV.CRM_DB_USER,
            CRM_DATABASE: EMPTY_DOT_ENV.CRM_DATABASE,
            CRM_DB_PASSWORD: EMPTY_DOT_ENV.CRM_DB_PASSWORD,
            CRM_DB_PORT: EMPTY_DOT_ENV.CRM_DB_PORT,
            CRM_DB_HOST: EMPTY_DOT_ENV.CRM_DB_HOST,
        };
        should(DOT_ENV);
        // Then
        expect(DOT_ENV.CRM_DB_USER).to.be.equal(undefined);
        expect(DOT_ENV.CRM_DATABASE).to.be.equal(undefined);
        expect(DOT_ENV.CRM_DB_PASSWORD).to.be.equal(undefined);
        expect(DOT_ENV.CRM_DB_PORT).to.be.equal(undefined);
        expect(DOT_ENV.CRM_DB_HOST).to.be.equal(undefined);

        done();
    });
    mocha.it("It should return a pg client", (done) => {
        // Given
        require("dotenv").config();
        // When
        void connectDb().then(
            // Then
            (client) => expect(client).to.be.an("object")
        );

        done();
    });
});
