/* eslint-disable camelcase */
const chai = require("chai");
const expect = chai.expect;

const chaiHttp = require("chai-http");
chai.use(chaiHttp);

const TestResponseUtils = require("../utils/response.utils");
const mocha = require("mocha");
const { should } = require("chai");

mocha.describe("Prospects", () => {
    const app = require("../../src/server");
    const baseUrl = "/api/v1";
    const prospects = "/prospects";

    mocha.describe("POST /prospects", () => {
        mocha.it("It should return a 200 code when creating a prospect", (done) => {
            //given
            const url = baseUrl + prospects;
            const payload = {
                id: 100000,
                name: "Integration test",
                display_name: "Integration test",
                email: "integration.test@test.test",
                active: true,
            };
            chai.request(app)
                .post(url)
                .send(payload)
                .end((err, res) => {
                    //when
                    const response = TestResponseUtils.parseJsonReponse(res.text);
                    //then
                    should(res);
                    res.should.have.status(200);
                    expect(response.message).to.equal("Prospect has been created");
                    done();
                });
        });
        mocha.it("It should return a 400 code if prospect already exists", (done) => {
            //given
            const url = baseUrl + prospects;
            const payload = {
                id: 100000,
                name: "Integration test",
                display_name: "Integration test",
                email: "integration.test@test.test",
                active: true,
            };
            chai.request(app)
                .post(url)
                .send(payload)
                .end((err, res) => {
                    //when
                    const response = TestResponseUtils.parseJsonReponse(res.text);
                    //then
                    should(res);
                    res.should.have.status(400);
                    expect(response.message).to.equal("An error has occured");
                    done();
                });
        });
    });
    mocha.describe("GET /prospects", () => {
        mocha.it("It should return a 200 code when fetching prospects", (done) => {
            //given
            const url = baseUrl + prospects;
            chai.request(app)
                .get(url)
                .end((err, res) => {
                    //when
                    const response = TestResponseUtils.parseJsonReponse(res.text);
                    //then
                    should(res);
                    res.should.have.status(200);
                    expect(response.length).should.not.be.equal(0);
                    done();
                });
        });
    });
    mocha.describe("PUT /prospects/:id", () => {
        mocha.it("It should return a 200 code when modifying prospects", (done) => {
            //given
            const url = baseUrl + prospects + "/100000"; // prospect created just above
            const payload = {
                name: "Integration test MODIFIED",
                email: "integration.test@test.test",
            };
            chai.request(app)
                .put(url)
                .send(payload)
                .end((err, res) => {
                    //when
                    const response = TestResponseUtils.parseJsonReponse(res.text);
                    //then
                    should(res);
                    res.should.have.status(200);
                    expect(response.message).to.equal("Propect has been modified");
                    done();
                });
        });
    });
    mocha.describe("DELETE /prospects/:id", () => {
        mocha.it("It should return a 200 code when deleting prospects", (done) => {
            //given
            const url = baseUrl + prospects + "/100000"; // prospect created just above
            chai.request(app)
                .delete(url)
                .end((err, res) => {
                    //when
                    const response = TestResponseUtils.parseJsonReponse(res.text);
                    //then
                    should(res);
                    res.should.have.status(200);
                    expect(response.message).to.equal("Propect has been deleted");
                    done();
                });
        });
        mocha.it("It should return a 200 code if prospect does not exist", (done) => {
            //given
            const url = baseUrl + prospects + "/100000"; // prospect created just above
            chai.request(app)
                .delete(url)
                .end((err, res) => {
                    //when
                    const response = TestResponseUtils.parseJsonReponse(res.text);
                    //then
                    should(res);
                    res.should.have.status(200);
                    expect(response.message).to.equal("Propect has been deleted");
                    done();
                });
        });
    });
});
