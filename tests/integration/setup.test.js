const chai = require("mocha");
const before = chai.before;
const after = chai.after;

before(async () => {
    console.log("Launching the integration tests...")
});

after(() => {
    console.log("End of integration test");
});
