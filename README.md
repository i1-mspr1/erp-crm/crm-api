# Web App

The aim is to provide an api for the CRM (CRUD on CRM's db)
## Authors

| LASTNAME  | FIRSTNAME  | 
|-----------|------------|
| DUHEM | Matthieu  |
| JAHAN | Robin  |
| RAOUL  | Antoine  |
| SAPIN  | Nicolas  |
| LECAPLAIN  | Adrien  |

## Project setup

Before all, add `.env` to run it locally at the root of the project

Then install the project with

```
npm install
```

## Definition of .env variables

| VARIABLE | TYPE | DEFAULT | DESCRIPTION |
|----------|------|---------|-------------|
| PORT | number  | 3000 | Sets the port for the http protocol |
| CRM_DB_HOST | string  | | IP of CRM db |
| CRM_DB_PORT | number  | | Port of CRM db |
| CRM_DB_USER | string  | | CRM db user |
| CRM_DB_PASSWORD | string  | | CRM db user's password |

## Compiles and hot-reloads for local

```
npm run start
```

## Lints and fixes files

```
npm run lint
```

## Security checking

```
npm run security:audit
```

## Tests

### Unit

```
npm run test:unit // for unit tests
```

### Integration

```
npm run test:integration // for integration tests
```