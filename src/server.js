const express = require("express");
const cors = require("cors");
const prospectRestRouter = require("./routes/prospect");
const connectDb = require("./db/config");

const SERVER = {
    PORT: process.env.PORT,
    BASE_URL: "/api/v1",
};

const app = express();

app.use(cors());
app.options("*", cors());

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const main = async () => {
    const client = await connectDb();
    app.set("client", client);

    app.use(SERVER.BASE_URL + "/", prospectRestRouter);

    app.listen(SERVER.PORT, () => {
        console.log(`Server is running on : ${SERVER.PORT}`);
    });
};

void main();
module.exports = app;
