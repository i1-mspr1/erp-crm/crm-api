/* eslint-disable no-undef */
require("dotenv").config();
const express = require("express");

const router = express.Router();

router.post("/prospects", async (req, res) => {
    try {
        const client = req.app.get("client");
        const { id, email, name } = req.body;
        if (id) {
            await client.query("INSERT INTO res_partner (id, name, display_name, email, active) VALUES ($1, $2, $2, $3, $4)", [id, name, email, true]);
        } else {
            await client.query("INSERT INTO res_partner (name, display_name, email, active) VALUES ($1, $1, $2, $3)", [name, email, true]);
        }
        res.status(200).json({ message: "Prospect has been created" });
    } catch (err) {
        res.status(400).json({ message: "An error has occured" });
        throw Error(`An error occurred while inserting the prospect: ${err}`);
    }
});

router.get("/prospects", async (req, res) => {
    try {
        const client = req.app.get("client");
        const prospects = await client.query("SELECT id, name, display_name, email FROM res_partner");
        res.status(200).json(prospects.rows);
    } catch (err) {
        res.status(400).json({ message: "An error has occured" });
        throw Error(`An error occurred while fetching the prospect: ${err}`);
    }
});

router.delete("/prospects/:id", async (req, res) => {
    try {
        const client = req.app.get("client");
        await client.query("DELETE FROM public.res_partner WHERE id=$1", [req.params.id]);
        res.status(200).json({ message: "Propect has been deleted" });
    } catch (err) {
        res.status(400).json({ message: "An error has occured" });
        throw Error(`An error occurred while deleting the prospect: ${err}`);
    }
});

router.put("/prospects/:id", async (req, res) => {
    try {
        const client = req.app.get("client");
        const { email, name } = req.body;
        await client.query("UPDATE public.res_partner SET name=$2, display_name=$2, email=$3 WHERE id=$1", [req.params.id, name, email]);
        res.status(200).json({ message: "Propect has been modified" });
    } catch (err) {
        res.status(400).json({ message: "An error has occured" });
        throw Error(`An error occurred while modifying the prospect: ${err}`);
    }
});

module.exports = router;
