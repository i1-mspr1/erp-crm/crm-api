const { Client } = require("pg");

const connectDb = async () => {
    const configDb = {
        user: process.env.CRM_DB_USER,
        database: process.env.CRM_DATABASE,
        password: process.env.CRM_DB_PASSWORD,
        port: process.env.CRM_DB_PORT,
        host: process.env.CRM_DB_HOST,
    };

    const client = new Client(configDb);
    await client.connect();
    return client;
};

module.exports = connectDb;
